// import dependencies you will use
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
//setting up Express Validator
const {check, validationResult} = require('express-validator'); // ES6 standard for destructuring an object

// setup databse connection - to connect front end application with back end database
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/isp',{
    useNewUrlParser: true,
    useUnifiedTopology: true   
});

// set up the model for the order 
const Order = mongoose.model('Order',{
            name : String,
            email : String,
            phone : String, 
            plan1 : Number,
            plan2 : Number,
            plan3 : Number,
            discount:Number,
            subTotal : Number,
            tax : Number,
            total : Number
            
});

// set up variables to use packages
var myApp = express();
myApp.use(bodyParser.urlencoded({extended:false}));

// set path to public folders and view folders

myApp.set('views', path.join(__dirname, 'views'));
//use public folder for CSS etc.
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine', 'ejs');
// set up different routes (pages) of the website

//home page
myApp.get('/', function(req, res){
    res.render('form'); // no need to add .ejs to the file name
});

//defining regular expressions
var nameRegex = /^[a-zA-Z]+$/;
var phoneRegex = /^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$/;

//function to check a value using regular expression
function checkRegex(userInput, regex){
    if(regex.test(userInput)){
        return true;
    }
    else{
        return false;
    }
}
// Custom phone validation function
function customPhoneValidation(value){
    if(!checkRegex(value, phoneRegex)){
        throw new Error('Phone should be in the format xxx-xxx-xxxx');
    }
    return true;
}
//function to check postalcode validation
function customNameValidation(value)
{
    if(!checkRegex(value, nameRegex))
    {
        throw new Error('name can be just characters')
    }
    return true;
}

//form submission handler
myApp.post('/', [
   check('name').custom(customNameValidation),
   check('phone').custom(customPhoneValidation),
   check('plan1', 'you should type a number for products number').isInt(),
   check('plan2', 'you should type a number for products number').isInt(),
   check('plan3', 'you should type a number for products number').isInt()
    
],function(req, res){

    const errors = validationResult(req);
    if (!errors.isEmpty()){
        //console.log(errors); // check what is the structure of errors
        res.render('form', {
            errors:errors.array()
        });
    }
    else{
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var plan1 = req.body.plan1;
        var plan2 = req.body.plan2;
        var plan3 = req.body.plan3;

        var subTotal = 0;
        subTotal += plan1 * 25.99;
        subTotal += plan2 *65.99;
        subTotal += plan3 *250;

        var discount = plan3*250*0.2;


        var tax = subTotal*0.13;


        var total = subTotal - discount + tax;

        var pageData = {
            name : name,
            email : email,
            phone : phone, 
            plan1 : plan1,
            plan2 : plan2,
            plan3 : plan3,
            subTotal : subTotal,
            discount : discount,
            tax : tax,
            total : total
        }

        // Create an object for the model Order,
        
        var myOrder = new Order(pageData);
        
        // save this order

        myOrder.save().then( ()=>{
            console.log('New order information saved in database');
        });
        
        res.render('form', pageData);
    }
});

// This is express route for all orders with data retrival procedure 
myApp.get('/allorders',function(req,res){
    Order.find({}).exec(function(err,orders){
        res.render('allorders',{orders:orders});
    });
});



//author page
myApp.get('/author',function(req,res){
    res.render('author',{
        name : 'Negin BZ',
        studentNumber : '8743698'
    }); 
});

// start the server and listen at a port
myApp.listen(8080);

//tell everything was ok
console.log('Everything executed fine.. website at port 8080....');


