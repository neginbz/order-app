## Overview 
It's an application for an Internet Service Provider that will connect to a
MongoDB database for storing all the orders. I used Node Module System, covers command line arguments and NPM packages.

## Technologies Used This app is built on: 
* [NodeJS](https://nodejs.org) 
* [npm](https://www.npmjs.com/) 
* [MongoDB](https://www.mongodb.com/) 

## Getting Started
You will need: * Node v16.13.2 * Npm v8.1.2

### To run the app locally Run the below commands: 
`npm i` - Install the packages in `package.json`<br>
`node index.js` - Run the program

## License & Copyright
licensed under the [MIT License](LICENSE) <br>
I used the MIT License besause it is very permissive, welcoming to open-source developers and businesses(Proprietary).<br>
It's actually both business-friendly and open-source friendly.
